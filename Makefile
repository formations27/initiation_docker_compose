# Variables ###############################################
PROJECT_NAME := OFDIG_initiation-a-docker-compose
LATEX_DIR := presentation
TEX_FILES := $(LATEX_DIR)/$(PROJECT_NAME)
###########################################################

# Presentation ############################################
all: presentation

presentation: $(TEX_FILES).pdf

$(TEX_FILES).pdf: $(TEX_FILES).tex
	pdflatex -output-directory $(LATEX_DIR) $(TEX_FILES).tex

$(TEX_FILES).tex:

clean:
	rm $(TEX_FILES).aux
	rm $(TEX_FILES).bcf
	rm $(TEX_FILES).log
	rm $(TEX_FILES).nav
	rm $(TEX_FILES).out
	rm $(TEX_FILES).run.xml
	rm $(TEX_FILES).snm
	rm $(TEX_FILES).toc
	rm $(TEX_FILES).pdf
###########################################################
