library(DBI)
library(RMariaDB)

con <- DBI::dbConnect(MariaDB(),
                      port = "3306",
                      host = "db",
                      user = "jane_doe",
                      password = "secretpassword",
                      dbname = "test"
)

dbListTables(con)
dbWriteTable(con, "mtcars", mtcars, overwrite = TRUE)
dbListTables(con)

# Disconnect from db
dbDisconnect(con)
