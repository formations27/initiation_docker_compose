<!-- LTeX: enabled=false -->
# Table des matières <!-- :TOC_3: -->
- [Mise en place du projet](#mise-en-place-du-projet)
  - [Installation des dépendances](#installation-des-dépendances)
  - [Récupération du projet](#récupération-du-projet)
- [Tutoriel de mise en place de services intégrés via `docker compose`](#tutoriel-de-mise-en-place-de-services-intégrés-via-docker-compose)
  - [Notions préliminaires](#notions-préliminaires)
    - [Qu'est-ce qu'un conteneur ?](#quest-ce-quun-conteneur-)
    - [Qu'est-ce que docker ?](#quest-ce-que-docker-)
    - [Qu'est-ce que docker compose ?](#quest-ce-que-docker-compose-)
    - [Contenu du tutoriel](#contenu-du-tutoriel)
  - [Définition de services à l'aide de docker compose](#définition-de-services-à-laide-de-docker-compose)
    - [Mise en place d'une base de donnée et d'un service d'interface web](#mise-en-place-dune-base-de-donnée-et-dun-service-dinterface-web)
    - [Mise en place d'un environnement de développement](#mise-en-place-dun-environnement-de-développement)
    - [Mise en place d'une base de donnée, d'un service de visualisation web et d'un environnement de développement](#mise-en-place-dune-base-de-donnée-dun-service-de-visualisation-web-et-dun-environnement-de-développement)
- [Trucs et astuces](#trucs-et-astuces)
  - [Obtenir des informations à propos de l'application (notamment pour débogage)](#obtenir-des-informations-à-propos-de-lapplication-notamment-pour-débogage)
  - [Stockage des identifiants et mots de passe à l'extérieur du fichier de configuration Compose](#stockage-des-identifiants-et-mots-de-passe-à-lextérieur-du-fichier-de-configuration-compose)
  - [Persistance du contenu de l'environnement de travail via la définition de `volume`](#persistance-du-contenu-de-lenvironnement-de-travail-via-la-définition-de-volume)
- [Documentation](#documentation)
  - [Docker](#docker)
  - [R et Docker](#r-et-docker)
  - [Guides méthodologiques du BIN/PHuN](#guides-méthodologiques-du-binphun)

# Mise en place du projet #
<!-- LTeX: enabled=true -->
## Installation des dépendances ##

Pour pouvoir utiliser le projet, deux dépendances sont nécessaires : 
- un service docker fonctionnel ;
- docker compose.

Pour les mettre en place, le plus simple consiste à installer `Docker Desktop` qui offre une interface graphique simple pour interagir avec les images. Pour ce faire, suivez les instructions d'installation appropriées pour votre système d'exploitation :
- [GNU/Linux](https://docs.docker.com/desktop/install/linux-install/)
- [Windows](https://docs.docker.com/desktop/install/windows-install/)
- [MacOS](https://docs.docker.com/desktop/install/mac-install/)

Pour les systèmes GNU/Linux, il est possible d'installer docker et docker compose sans Docker Desktop en [mettant en place le service](https://docs.docker.com/engine/install/), puis en [installant directement le paquet pour docker compose](https://docs.docker.com/compose/install/linux/#install-using-the-repository).

## Récupération du projet ##

Cliquer sur le button `Code` tel qu'indiqué sur l'image ci-dessous 
<!-- ![Alt text](images/telechargement_code.png "Button pour téléchargement du code") -->
<p align="center">
<img src="images/telechargement_code.png" alt="placeholder" width="75%" height="75%">
</p>
puis, choisissez l'option de téléchargement qui vous convient.

Avec `git` en ligne de commande :

```shell
git clone https://gitlab.com/formations27/initiation_docker_compose.git
```

# Tutoriel de mise en place de services intégrés via `docker compose` #
## Notions préliminaires ##

Docker compose est une solution logicielle permettre de définir des applications multiconteneurs.

### Qu'est-ce qu'un conteneur ? ###
Un conteneur est une entité isolée dans une architecture informatique qui inclut une application et son environnement permettant son exécution indépendamment d'autres applications. Les conteneurs sont souvent utilisés pour simplifier la distribution, l'exécution et la maintenance de multiples applications dans un seul système.
(*rédigé avec l'aide d'[Ollama mistral](https://ollama.com/library/mistral)*)


### Qu'est-ce que docker ? ###
Docker est une plateforme de gestion de conteneur qui permet de construire, de distribuer et d'exécuter des applications en isolant chaque partie d'un système dans un conteneur. Elle permet, entre autre, de faciliter la collaboration entre développeurs, la gestion des dépendances et la mise à l'échelle des applications
(*rédigé avec l'aide d'[Ollama zephyr](https://ollama.com/library/zephyr)*)

### Qu'est-ce que docker compose ? ###
Docker compose est un outil open source conçu pour faciliter la gestion et la coordination des applications en conteneurs Docker. Il permet aux développeurs de décrire et d'exécuter plusieurs conteneurs dans un seul fichier `YAML` en spécifiant les ressources requises telles que les images, les réseaux et les volumes. Docker compose simplifie l'administration des conteneurs en automatisant les étapes de construction, de démarrage et de mise à jour de plusieurs conteneurs simultanément. De plus, il offre un ensemble d'outils pour la gestion des dépendances entre les conteneurs, la génération d'un réseau privé isolé pour eux, ainsi que la possibilité de redémarrer tous les conteneurs d'une application en une seule commande. 
(*rédigé avec l'aide d'[Ollama zephyr](https://ollama.com/library/zephyr)*)

### Contenu du tutoriel ###

La mise en place de trois types de services différents sera exploré dans le cadre de ce tutoriel :
- [une base de donnée (`mariadb`)](https://mariadb.org/)
- [une interface web de base de donnée (`adminer`)](https://www.adminer.org/)
- [un environnement de développement (`rstudio`)](https://posit.co/products/open-source/rstudio/)

Diverses notions en lien avec `docker compose` seront abordées se faisant (sans prétention d'exhaustivité) :
- La manipulation de conteneurs via la commande `docker compose` 
- La configuration et la coordination d'un ensemble de services dans une même fichier de configuration `docker compose.yml` 
- Divers trucs et astuces (débogage, personnalisation des images via `Dockerfile`, le stockage des informations secrète à l'extérieur du fichier de configuration `.env`, etc.)
- ...

## Définition de services à l'aide de docker compose ##

### Mise en place d'une base de donnée et d'un service d'interface web ###

Un fichier de configuration type pour la mise en place de ce type de service ressemble à celui-ci : 
``` yaml
services:

  db:
    image: mariadb
    restart: always
    environment:
      MARIADB_ROOT_PASSWORD: supersecretpassword 

  webdb:
    image: adminer
    restart: always
    ports:
      - "8080:8080"
```
Dans ce fichier sont définis deux services :
1. la base de donnée *per se* (`mysql`, implémentation `mariadb`) et 
2. un explorateur web de base de donnée (`adminer`) pour interagir avec la base de donnée. 

> Vous pouvez explorer les différentes images accessibles sur [docker hub](https://hub.docker.com/).

Une fois le fichier de configuration créé (e.g. [src/mariadb_adminer.yaml](src/mariadb_adminer.yaml)), vous pouvez lancer la base de donnée à l'aide de la commande suivante :

``` shell
docker compose -f src/mariadb_adminer.yaml up -d
```

La commande est à interpréter de la façon suivante :
- L'option `compose` est nécessaire pour que docker considère le fichier Compose. 
- L'argument `-f` pour `--file` permet d'indiquer le fichier Compose à utiliser par docker (le fichier par défaut étant `docker compose.yml`).
- L'argument indique à docker compose de lancer les divers élément des conteneurs (`down` permet de les arrêter) 
- L'argument `-d` pour `--detach` permet de lancer le service en arrière-plan et de récupérer le terminal. 

> Pour explorer les options de docker, lancez `docker compose --help` ou `man docker compose` pour consulter la documentation.

Vous pouvez vérifier que la base de donnée est bien fonctionnelle en vous connectant à celle-ci via le port approprié à l'adresse suivante : http://localhost:8080

> Attention ! Si votre système utilise déjà l'un des ports, le lancement de la commande renverra une erreur. Pour régler le problème, changez simplement le port spécifier dans le fichier Compose.

Vous devriez arriver sur la page suivante. 
<!-- ![Alt text](images/adminer_login_page.png "Page d'accueuil adminer") -->
<p align="center">
<img src="images/adminer_login_page.png" alt="placeholder" width="75%" height="75%">
</p>

Compléter les informations de connexion vérifier que tout fonctionne. Vous devriez arriver à la page suivante.
<!-- ![Alt text](images/adminer_login_success.png "Page d'accueuil connexion réussie adminer") -->
<p align="center">
<img src="images/adminer_login_success.png" alt="placeholder" width="75%" height="75%">
</p>

### Mise en place d'un environnement de développement ###
#### Qu'est-ce que `R` ? ####

R est un langage de programmation scientifique largement utilisé pour l'analyse de données de même que la visualisation de donnée. Il est particulièrement utilisé dans l'analyse statistique, bien que sa large bibliothèque de paquet permettent de réaliser la grande majorité des tâches typiquement associées à l'analyse de données (analyse réseaux, analyse de texte, apprentissage machine, etc).

#### Qu'est-ce que `Rstudio` ? ####

`RStudio` est un environnement de développement intégré pour le langage
de programmation R (plus récemment, fournit un soutien limité pour python). Il permet aux utilisateurs de créer, exécuter et déboguer des scripts en R de manière intuitive et collaborative grâce à ses fonctionnalités avancées telles que l'éditeur de texte avec mise en évidence de syntaxe, les panneaux d'aide et d'historique, un débogueur interactif, la gestion des packages et des bibliothèques, ainsi qu'une vaste galerie d'extensions pour augmenter ses fonctionnalités.
(*rédigé avec l'aide d'[Ollama zephyr](https://ollama.com/library/zephyr)*)

#### Mise en place comme service ####

Un fichier de configuration type pour la mise en place de ce type de service ressemble à celui-ci : 
``` yaml
services:
  rstudio:
    image: rocker/rstudio
    ports:
      - "8787:8787"
    environment:
      PASSWORD: supersecretpassword2
```

Une fois le fichier de configuration créé (e.g. [src/rstudio.yaml](src/rstudio.yaml)), vous pouvez lancer l'environnement de développement en lançant simplement la commande suivante référant au fichier

``` shell
docker compose -f src/rstudio.yaml up -d
```

Vous pouvez vérifier que `rstudio` est bien fonctionnelle en vous connectant à celui-ci via le port approprié à l'adresse suivante : http://localhost:8787

Vous devriez arriver sur la page suivante. 
<!-- ![Alt text](images/rstudio_login_page.png "Page d'accueuil rstudio") -->
<p align="center">
<img src="images/rstudio_login_page.png" alt="placeholder" width="75%" height="75%">
</p>

Compléter les informations de connexion vérifier que tout fonctionne. Vous devriez arriver à la page suivante.
<!-- ![Alt text](images/rstudio_login_success.png "Page d'accueuil connexion réussie rstudio") -->
<p align="center">
<img src="images/rstudio_login_success.png" alt="placeholder" width="75%" height="75%">
</p>


### Mise en place d'une base de donnée, d'un service de visualisation web et d'un environnement de développement ###

Imaginons ici, que vous souhaitiez configurer un environnement de développement ayant accès 
à une base de donnée, ayant une interface web pour explorer les données et un outil de développement permettant l'analyse et la visualisation des données stockées dans la base de donnée. 

Un fichier de configuration type pour la mise en place de ce type de service ressemble à celui-ci : 
``` yaml
services:
  db:
    image: mariadb
    restart: always
    environment:
      MARIADB_ROOT_PASSWORD: supersecretpassword 

  webdb:
    image: adminer
    restart: always
    ports:
      - "8080:8080"

  rstudio:
    image: rocker/rstudio
    ports:
      - "8787:8787"
    environment:
      PASSWORD: supersecretpassword2
```

Comparativement aux configurations précédentes, des dépendances supplémentaires sont nécessaires pour permettre l'interaction avec la base de donnée à l'aide de `R` (i.e. `libmysqlclient-dev`). Pour ce faire, plutôt que d'appeler directement `rocker/rstudio`, un fichier de configuration `Dockerfile` sera rédigé pour personnaliser l'image désirée.

Un fichier `Dockerfile` permet de définir une séquence d'étape à suivre pour la création du conteneur à partie d'une image prédéfinie (ou même de définir une image). L'exemple suivant présente une partie de la syntaxe : 

``` dockerfile
FROM rocker/rstudio

RUN  apt-get -y update && \
     apt-get install -y --no-install-recommends \
     libmysqlclient-dev

RUN R -e 'install.packages("DBI")'
RUN R -e 'install.packages("RMariaDB")'
RUN R -e 'install.packages("caret")'
RUN R -e 'install.packages("ggplot2")'
```

> Voir la [documentation](https://docs.docker.com/reference/dockerfile/) pour une explicitation de la syntaxe.

Une fois défini, ce fichier ([voir src/Dockerfile_rstudio](src/Dockerfile_rstudio)) peut-être appelé par docker compose à la place de l'image utilisée initialement pour s'assurer que le conteneur contient les dépendances nécessaires pour exécuter le code.
``` yaml
services:
  db:
    image: mariadb
    restart: always
    environment:
      MARIADB_ROOT_PASSWORD: supersecretpassword 
      MARIADB_USER: jane_doe
      MARIADB_PASSWORD: secretpassword
      MARIADB_DATABASE: test
    ports:
      - "3636:3306"
    volumes:
      - db_data:/var/lib/mysql

  webdb:
    image: adminer
    restart: always
    ports:
      - "8080:8080"

  rstudio:
    build:
      context: .
      dockerfile: Dockerfile_rstudio
    ports:
      - "8787:8787"
    environment:
      PASSWORD: supersecretpassword2
    volumes:
      - ./scripts:/root/scripts

volumes:
  db_data:
    name: "tutorial-db-data"
```

Cette nouvelle définition ([voir src/mariadb_adminer.yaml](src/mariadb_adminer_rstudio.yaml)) peut-être lancée de la même façon de précédemment :

``` shell
docker compose -f src/mariadb_adminer_rstudio.yaml up -d
```
Ici encore, le port 8787 peut être utilisé pour vous connecter via l'adresse suivante : http://localhost:8787

> Les sections `volumes` comprennent ici des arguments qui seront présentés dans la section [Persistance du contenu de l'environnement de travail via la définition de `volume`](#persistance-du-contenu-de-lenvironnement-de-travail-via-la-définition-de-volume).

# Trucs et astuces #
## Obtenir des informations à propos de l'application (notamment pour débogage)

Deux commandes propres à `docker compose` sont particulièrement utiles pour s'enquérir de l'état des conteneurs définit par une configuration particulière. 

La première, `docker compose -f ficher-de-configuration-compose.yaml ps` expose l'état des conteneurs créés. 

Par exemple, après avoir lancé `docker compose -f src/rstudio.yaml up -d`, si vous lancez `docker compose -f src/rstudio.yaml ps`, vous obtiendrez les informations suivantes :

```shell
CONTAINER ID  IMAGE                            COMMAND     CREATED        STATUS        PORTS                   NAMES
a464e33c496b  docker.io/rocker/rstudio:latest  /init       3 seconds ago  Up 3 seconds  0.0.0.0:8787->8787/tcp  src_rstudio_1
```
Plusieurs informations d'intérêt peuvent être extraites. Nous nous attarderons à deux :
- STATUS : Présente le status et le nombre de secondes associées à ce status (ici `Up` depuis 3 secondes)
- PORTS : Indique les ports, du réseau interne à l'application conteneurisée vers votre machine.

La première est intéressante en ce qu'elle permet d'identifier notamment les cas où votre application redémarre continuellement (nombre de secondes pour le status sera réinitialisé, et donc, le nombre de secondes sera toujours bas).

La seconde permet plutôt de s'assurer que nous nous tentons d'accéder au bon port pour notre application. Par exemple, si la configuration de `rstudio` dans [src/rstudio.yaml](src/rstudio.yaml) pointait vers le port externe `8887` plutôt que `8787`, comme ceci :
```yaml
	...
	ports:
	  - "8887:8787"
	...
```
alors, la bonne adresse d'accès serait http://localhost:8887 plutôt que http://localhost:8787.
Cette information serait montrée de la façon suivante par la commande :

```shell
CONTAINER ID  IMAGE                            COMMAND     CREATED        STATUS        PORTS                   NAMES
6449428e2c2b  docker.io/rocker/rstudio:latest  /init       3 seconds ago  Up 3 seconds  0.0.0.0:8887->8787/tcp  src_rstudio_1
```

La seconde commande d'intérêt pour explorer les problèmes rencontrés par votre application est `docker compose -f ficher-de-configuration-compose.yaml logs`. 

La sortie ne sera pas présentée ici, mais elle rapporte différentes informations associées au service lancé.
Il est aussi possible de limiter les logs à un (ou plusieurs) service(s) spécifique(s) en ajoutant leur(s) nom(s), e.g. `docker compose -f ficher-de-configuration-compose.yaml logs service1 service2 ...`  

Comparez les sorties suivantes après avoir lancé `docker compose -f src/mariadb_adminer.yaml up -d` :
- `docker compose -f src/mariadb_adminer.yaml logs`
- `docker compose -f src/mariadb_adminer.yaml logs webdb`
- `docker compose -f src/mariadb_adminer.yaml logs db`

<!-- TODO: Trouver infos similaire pour docker de base -->

## Stockage des identifiants et mots de passe à l'extérieur du fichier de configuration Compose ##

Définir vous identifiants et mots de passe dans le fichier de configuration Compose présente des problèmes de sécurité. Une solution est de séparer la définition de ceux-ci de la définition des services. Ce faisant, vous pouvez décider quels éléments vous partagez et par quels moyens.

`docker compose` va automatiquement considéré les fichiers `.env` avoisinant le fichier Compose (l'argument `--env-file` vous permet de spécifier manuellement la position du fichier).

La définition révisée du service `rstudio` en appelant les informations de connexion dans le fichier `.env` (voir fichier [src/rstudio_w_env.yaml](src/rstudio_w_env.yaml)).

[`.env`](.env)
``` shell
RTUDIO_PASSWORD=supersecretpassword4
```

[`rstudio_w_env.yaml`](rstudio_w_env.yaml)
``` yaml
services:
  rstudio:
    image: rocker/rstudio
    ports:
      - "8787:8787"
    environment:
      PASSWORD: ${RTUDIO_PASSWORD}
```

## Persistance du contenu de l'environnement de travail via la définition de `volume` ##
*À venir*

<!-- LTeX: enabled=false -->
# Documentation #
## Docker
- [Entrée wiki Conteneurs](https://en.wikipedia.org/wiki/Containerization_(computing))
- [Entrée wiki Docker](https://en.wikipedia.org/wiki/Docker_(software))
- [Docker Compose Quickstart](https://docs.docker.com/compose/gettingstarted/)
- [Docker for Beginners: Everything You Need to Know](https://www.howtogeek.com/733522/docker-for-beginners-everything-you-need-to-know/)
- [Set environment variables within your container's environment](https://docs.docker.com/compose/environment-variables/set-environment-variables/)
- [Docker images vs container](https://phoenixnap.com/kb/docker-image-vs-container)
- [Overview of installing Docker Compose](https://docs.docker.com/compose/install/)
- [How To Use Docker Desktop To Deploy Docker Containerized Applications] (https://www.geeksforgeeks.org/how-to-use-docker-desktop-to-deploy-docker-containerized-applications/)
## R et Docker
- [Rstudio](https://posit.co/products/open-source/rstudio/)
- [The Rocker Project : Docker Containers for the R Environment](https://rocker-project.org/)
- [Rstudio, tidyverse, verse, geospatial (Rocker Project)](https://rocker-project.org/images/versioned/rstudio.html)
- [An Introduction to Rocker : Docker Containers for R](https://doi.org/10.32614/RJ-2017-065)
- [The Rockerverse: Packages and Applications for Containerisation with R](https://doi.org/10.32614/RJ-2020-007)
## Guides méthodologiques du BIN/PHuN
- [Comment coder proprement](https://cirst.uqam.ca/outils_bin/guide-methodologique-du-bin-phun-pour-les-humanites-numeriques-comment-coder-proprement/)
- [L'importance des tests](https://cirst.uqam.ca/outils_bin/guide-methodologique-du-bin-phun-pour-les-humanites-numeriques-limportance-des-tests/)
- [git – les bonnes pratiques](https://cirst.uqam.ca/outils_bin/git-les-bonnes-pratiques/)
- [Organiser son projet](https://cirst.uqam.ca/outils_bin/3948/)
